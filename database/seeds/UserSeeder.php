<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
         'name' => 'Pemilik Toko',
         'username'=> 'superuser',
         'email' => 'superuser@localhost.com',
         'password' => bcrypt('password'),
      ]);

      DB::table("roles")->insert([
       'name'=>'administrator',
       'description'=>'can access all features'
      ]);

      DB::table("roles")->insert([
       'name'=>'user',
       'description'=>'some feature will be disable'
      ]);

      DB::table("user_roles")->insert([
        "user_id"=>1,
        "role_id"=>1
      ]);
    }
}
