<?php
namespace App\Http;

class ResponseData
{
    public $message;
    public $success;
    public $data;
    public $status;

    /**
     * ResponseData constructor.
     */
    public function __construct()
    {
        $this->success = false;
        $this->status = 400;
        $this->message = "Opps, Kamu tersesat kawan?";
        $this->data = new \stdClass();
    }

}
