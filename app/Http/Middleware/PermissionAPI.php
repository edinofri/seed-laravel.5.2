<?php

namespace App\Http\Middleware;

use Closure;
use Request;
use Firebase\JWT\JWT;

class PermissionAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->isValid($request->header('Authorization'))){
          return response(["success"=>false,"message"=>"Unauthorized","error"=>401],401);
        }
        return $next($request);
    }
    private function isValid($rawToken){
      $JWT_KEY = "MXV46XxCFRxNuB8LyAtmLDgixRnTAlMHjSACddwkyKem88eZtw9fzxz";
      $expired_token = "2"; // in hours
      $_sign = 'cisabgnakut';
      try{
        $sign = explode(':',$rawToken)[0];
        $token = explode(':',$rawToken)[1];
        if($_sign != $sign){
          return false;
        }
        $encode_token = JWT::decode($token,$JWT_KEY,array('HS256'));
        return true;
      }catch(\Exception $e){
        return false;
      }
      return false;
    }
}
