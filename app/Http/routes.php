<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('api/register','UserController@create');
Route::post('api/login','UserController@login');

Route::group(['prefix'=>'api','middleware'=>'permission_api'],function(){
  Route::get('test','UserController@test');

});

Route::group(['prefix'=>'administrator','middleware'=>'permission_administrator'],function(){
  Route::get('/','AdministratorController@index');
  Route::get('/users','AdministratorController@users');
});

Route::group(['prefix'=>'api','middleware'=>'permission_administrator'],function(){
    Route::get('/users','UserController@index');
});


Route::group(['middleware'=>'csrf'],function(){
  Route::auth();
});


Route::get('/home', 'HomeController@index');
