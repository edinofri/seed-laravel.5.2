<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/flag-icon.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://coreui.io/demo/pro/Ajax_Demo/vendors/css/simple-line-icons.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/core-ui.css">
</head>
<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
            <form class="card-body" role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}
              <h1>Login</h1>
              <p class="text-muted">Sign In to your account</p>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="icon-user"></i></span>
                </div>
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="icon-lock"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Password" name="password">
              </div>
              <div class="row">
                <div class="col-12">
                  @if (count($errors) > 0)
                  <span class="alert alert-danger" role="alert">
                    {{ $errors->first() }}
                  </span>
                  <br><br>
                  @endif
                </div>
                <div class="col-6">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="remember"> Remember Me
                      </label>
                  </div>
                  <input type="submit" class="btn btn-primary px-4" value="Login" />
                </div>
                <div class="col-6 text-right">
                  <a class="btn btn-link px-0" href="{{ url('/password/reset') }}">Forgot password?</a>
                </div>
              </div>
            </form>
          </div>
          <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
            <div class="card-body text-center">
              <div>
                <h2>Sign up</h2>
                <p>Belum punya akun? Silahkan daftar terlebih dahulu.</p>
                <a class="btn btn-primary active mt-3" href="{{ url('/register') }}">Register Now!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/jquery.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/popper.min.js"></script>
  <script src="https://coreui.io/demo/pro/Ajax_Demo/vendors/js/bootstrap.min.js"></script>
  <script src="/assets/js/app.js"></script>

</body>
</html>
