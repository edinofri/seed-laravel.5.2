<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/administrator"><i class="icon-speedometer"></i> Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/administrator/users"><i class="icon-user"></i> Users</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/logout"><i class="icon-lock"></i> Logout</a>
      </li>
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
